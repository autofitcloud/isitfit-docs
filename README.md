# isitfit.autofitcloud.com

This is the content behind https://isitfit.autofitcloud.com

It's basically all in

- the `mkdocs.yaml` file
- the files in `docs/`


This static site is built with [mkdocs](https://www.mkdocs.org/).
It is continuously deployed with https://netlify.com

Usage

```
# install
pip3 install mkdocs isitfit

# check locally
mkdocs serve

# build static files
mkdocs build

# update synopsis page
cd ../isitfit # CLI repo
/bin/sh synopsis_update.sh > ../isitfit-docs/docs/synopsis.md
```

Netlify will automatically build the site with the `./netlify.toml` file
