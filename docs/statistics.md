# Statistics and Usage Tracking

What data we collect and why

---

`isitfit` seeks to be driven by the usage and demand of the community.

We observe what users are doing by collecting various events and usage data,
and we use this data to iterate and improve `isitfit` based on this gained insight.
This includes things like the installed version of `isitfit` and the commands being used.

We do not use event payloads to collect any identifying information,
and the data is used in aggregate to understand the community as a whole.

Here is an example of a collected event:

```
isitfit version:  0.10
Command issued:   cost analyze
Options used:     --n-days=7
Installation ID:  abcdef123456 (random per installation)
```

To collect these events, we use Matomo Cloud, whose privacy policy can be found [here](https://matomo.org/privacy/)
(scroll down to the section *Ways Matomo protects the privacy of your users and customers*).
Matomo attaches the following information to each collected event:

```
Date/Time:        2019-10-05 12:34
IP address:       123.123.0.0 # (anonymized)
IP city:          Jacksonville
IP country:       USA
```

If you use the `--share-email` option,
the email address is stored by [AWS SES](https://aws.amazon.com/ses/)
to remember that it has already been verified.
Prior to 2019-12-30, only the email's timestamp was stored to prevent abuse of the `--share-email` service.
As of 2019-12-30, the email's contents are also stored to help us evaluate the usefulness of `isitfit` to users
and potentially improve `isitfit` with any gained insight.


If you have any comments, feedback, or special needs about these statistics and usage tracking,
please get in touch by filling out the form at https://autofitcloud.com/contact and we'll figure out a solution together.


(This section was adapted from [serverless](https://serverless.com/framework/docs/providers/aws/cli-reference/slstats/))
