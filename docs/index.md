# isitfit <img src="images/isitfit.png" width=100 />

Fast, free, and open-source rightsizing of AWS accounts. 

---

`isitfit` makes it easier & faster to scan an AWS account for oversized or idle EC2 instances or Redshift clusters.
This is a topic that can directly affect your costs, but which often gets ignored until your CFO tunes in.
With `isitfit`, you could identify rightsizing opportunities in less than 5 minutes regardless of the infrastructure size.
It gets CPU utilization metrics from AWS Cloudwatch, and it can get memory metrics from Datadog if available.


## Quickstart


Install with python's pip

```
pip3 install isitfit
```

Install with docker

```
docker pull autofitcloud/isitfit:latest

# drop within the docker container's terminal, where you can run `isitfit` commands
docker run -it -v ~/.aws:/root/.aws autofitcloud/isitfit:latest bash
```

Here's the list of implemented commands:

Action | Command
------------ | -------------
Measure AWS account EC2/Redshift sizing efficiency | `isitfit cost analyze`
Ditto, with filtering on tags|`isitfit --filter-tags=something cost analyze`
Identify oversized EC2 instances or Redshift clusters | `isitfit cost optimize`
Ditto, with filtering on tags|`isitfit --filter-tags=something cost optimize`
Dump tags from AWS EC2 to CSV|`isitfit tags dump` (yields name of CSV file in console)
Get tag suggestions from EC2 names|`isitfit tags suggest`
Push tags from CSV to AWS EC2|`isitfit tags push file.csv`
prints help message | `isitfit --help`

To schedule a call to the `isitfit cost analyze` command from cron to send you a weekly email every Monday morning at 9 am of your last week cost-weighted utilization, use:

```
0 9 1 * * * isitfit --share-email=shadi@autofitcloud.com cost --profile=default --ndays=7 analyze
```

For further details, check the [Concepts page](concepts.md), [Installation page](installation.md), or [Usage page](usage.md).


## License

Apache License 2.0. Check file [LICENSE](https://github.com/autofitcloud/isitfit/blob/master/LICENSE)


## Author

`isitfit` is brought to you by [AutofitCloud](https://www.autofitcloud.com),
seeking to cut cloud waste on our planet  🌎.

If you like `isitfit`, sign up for our pilot program
to bring more cost savings to your AWS budget
while improving `isitfit` through your use cases.

- The pilot program sign up form is at the bottom of [AutofitCloud's homepage](https://www.autofitcloud.com).
- To send a custom message, you can use [this contact form](https://www.autofitcloud.com/contact).
- To follow news and version announcements, check our [subreddit](https://reddit.com/r/autofitcloud)  or  [twitter account](https://twitter.com/autofitcloud)

--

[Shadi Akiki](http://www.teamshadi.net/)

Founder and CEO

AutofitCloud Technologies, Inc.



## Security

<!-- inspired from https://github.com/pytest-dev/pytest-mock/#security-contact-information -->

To report a security vulnerability, please use the AutofitCloud [contact page](https://autofitcloud.com/contact).

We will coordinate the fix and disclosure.


<br>

<div class='row'>
  <style>
  .announcement {
    color:rgba(255,255,255,.9);
    background:#0e1e25;
    padding:.236em .618em;
    border-radius: 4px;
    font-size:1em;
    font-weight: normal;
    display:inline-block;
  }
  .announcement strong {
    color:rgba(255,255,255,.9);
  }
  </style>

  <!-- col-md-offset-4 was renamed to offset-md-8 between bootstrap 3 and 4 -->
  <div class="col-md-2 col-md-offset-2">
    <a href="https://twitter.com/autofitcloud">
      <img src="/images/twitter.png" alt="twitter" width="30px">
    </a>
    &nbsp;
    <a href="https://www.reddit.com/r/autofitcloud">
      <img src="/images/reddit.png" alt="reddit" width="30px">
    </a>
    &nbsp;
    <a href="https://discord.gg/56zmfDc">
      <img src="/images/discord.png" alt="discord" width="30px">
    </a>
  </div>

  <div class="col-md-4">
    
  <p class="announcement">
    <strong>Pilot program?</strong>
    <br>
    Sign up on <a href="https://www.autofitcloud.com/">autofitcloud.com</a> →
  </p>
  </div>
</div>
