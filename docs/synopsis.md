# Synopsis

A list of all commands and options with isitfit


---


## `isitfit  --help`

```
Usage: isitfit [OPTIONS] COMMAND [ARGS]...

Options:
  --debug               Display more details to help with debugging
  --verbose             Display more details to help with understanding
                        program flow
  --share-email TEXT    Share result to email address
  --skip-check-upgrade  Skip step for checking for upgrade of isitfit
  --skip-prompt-email   Skip the prompt to share result to email address
  --help                Show this message and exit.

Commands:
  cost     Evaluate AWS EC2 costs
  datadog  Datadog utilities
  tags     Explore EC2 tags
  version  Show isitfit version
```


## `isitfit version --help`

```
Usage: isitfit version [OPTIONS]

  Show isitfit version

Options:
  --help  Show this message and exit.
```


## `isitfit cost --help`

```
Usage: isitfit cost [OPTIONS] COMMAND [ARGS]...

  Evaluate AWS EC2 costs

Options:
  --filter-region TEXT  specify a single region against which to run cost
                        analysis/optimization
  --profile TEXT        Use a specific profile from your credential file.
  --ndays TEXT          number of days to look back in the data history
  --help                Show this message and exit.

Commands:
  analyze   Analyze AWS EC2 cost
  optimize  Generate recommendations of optimal EC2 sizes
```


## `isitfit cost analyze --help`

```
Usage: isitfit cost analyze [OPTIONS]

  Analyze AWS EC2 cost

Options:
  --filter-tags TEXT  filter instances for only those carrying this value in
                      the tag name or value
  --save-details      Save details behind calculations to CSV files
  --help              Show this message and exit.
```


## `isitfit cost optimize --help`

```
Usage: isitfit cost optimize [OPTIONS]

  Generate recommendations of optimal EC2 sizes

Options:
  --n INTEGER                     number of underused ec2 optimizations to
                                  find before stopping. Skip to get all
                                  optimizations
  --filter-tags TEXT              filter instances for only those carrying
                                  this value in the tag name or value
  --allow-ec2-different-family / --forbid-ec2-different-family
                                  Allow suggesting EC2 instance types of
                                  different families
  --help                          Show this message and exit.
```


## `isitfit tags --help`

```
Usage: isitfit tags [OPTIONS] COMMAND [ARGS]...

  Explore EC2 tags

Options:
  --profile TEXT  Use a specific profile from your credential file.
  --help          Show this message and exit.

Commands:
  dump     Dump existing EC2 tags in tabular form into a csv file
  push     Push EC2 tags from csv file
  suggest  Generate new tags suggested by isitfit for each EC2 instance
```


## `isitfit tags dump --help`

```
Usage: isitfit tags dump [OPTIONS]

  Dump existing EC2 tags in tabular form into a csv file

Options:
  --help  Show this message and exit.
```


## `isitfit tags suggest --help`

```
Usage: isitfit tags suggest [OPTIONS]

  Generate new tags suggested by isitfit for each EC2 instance

Options:
  --advanced  Get advanced suggestions of tags. Requires login
  --help      Show this message and exit.
```


## `isitfit tags push --help`

```
Usage: isitfit tags push [OPTIONS] CSV_FILENAME

  Push EC2 tags from csv file

Options:
  --not-dry-run  True for dry run (simulated push)
  --help         Show this message and exit.
```


